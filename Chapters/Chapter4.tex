% Chapter Template

\chapter{Price Foresting} % Main chapter title

\label{Chapter4} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

The objective of this chapter is to establish a best way to forecast the order book prices $\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$. I initially carry some analysis to establish some properties of the system. I then suggest a good way to handle the 50 bid and 50 ask prices. The reason for the is we wish to forecast the behaviour of the full bid and ask curves. Appendix ~\vref{AppendixB} includes many of the Listings for this chapter.

%----------------------------------------------------------------------------------------
%	SECTION 1 - Initial Analysis
%----------------------------------------------------------------------------------------

\section{Initial Analysis} \label{sec:bid_ask_spread_modelling}
I start by modelling the bid-ask spread, $s_t$, as defined in Section ~\vref{sec:definitions} as it seems that this variable is autocorrelated based on Figure ~\vref{fig:best_bid_ask_spread_over_time}, because there is no obvious time trend in the series. This modelling effort should be a useful building block for the project.


\subsection{Unit Root Test} \label{sec:unit_root_test}

I start by checking whether or not the bid-ask spread as presented in Figure ~\vref{fig:best_bid_ask_spread_over_time} is stationary using Augmented Dickey-Fuller test (see \cite{Greene2003} or \cite{Wooldridge2012}). This test provides a quick indication of the stationarity in the time series and the model is

\begin{equation}
    \begin{split}
        \Delta s_{t} = \mu + \gamma s_{t-1} + \sum_{i=1}^{h} \phi_{i} \Delta s_{t-i} + \epsilon_t, \quad t = 1, \dots, T \\
        \epsilon_t \sim N(0, \sigma^2) \quad \text{and}  \quad Cov[\epsilon_t, \epsilon_z] = 0 \quad \forall t \neq z
    \end{split}
    \label{eqn:adf_model}
\end{equation}
with
\begin{equation}
    H_0: \gamma = 0 \quad \text{vs} \quad H_1: \gamma < 0
    \label{eqn:adf_model_hypothesis}
\end{equation}

$\gamma=0$ means that the process is non-stationary while $\gamma < 0$ suggests stationarity. $y_t$ is the bid-ask spread, $\mu$ is a constant term, $\gamma$ is the parameter of interest, $\phi_i$ are the parameters allowing for serial autocorrelation not to be present the error terms $\epsilon_t$. I used AIC minimisation as the method to choose the number of lag $h$ as suggested in \cite{Greene2003}.

Table ~\vref{tab:adf_test} shows the results for the ADF test for the bid-ask spread time series. 
\begin{table}[H]
    \caption{Augmented Dickey-Fuller Test Results - Price Forecasting}
    \centering
    \input{Tables/adf_test}
    \label{tab:adf_test}
\end{table}

We reject $H_0$ and establish the result that the bid-ask spread is stationary without any differencing. This test is based on Sample I with the 2-second frequency data. We can proceed and try to find the model for the stationarity of bid-ask spread series.

\subsection{ARIMA Model}

I used a Python package called \texttt{pmdarima} by \cite{Smith2020} to establish a suitable ARIMA(2, 0, 1) model the process, $s_t$, as
\begin{gather} 
         s_{t} = \mu + \phi_1 s_{t-1} + \phi_2 s_{t-2} + \theta_1 \epsilon_{t-1} + \epsilon_t \\
         \epsilon_t \sim N(0, \sigma^2) \quad \text{and}  \quad Cov[\epsilon_t, \epsilon_z] = 0 \quad \forall t \neq z
    \label{eqn:arima_model}
\end{gather} 
The package iterates over a set different ARIMA models based on initial specifications and picks up the best fit based on AIC. The results are shown in Listing ~\vref{lst:arima}. All model parameters ($\mu,\ \phi_1,\ \phi_2,\ \theta_1\ \text{and}\ \sigma^2$) are statistically significant.

\lstinputlisting[
        label={lst:arima},
        basicstyle=\ttfamily,
		language=Python,
		numbers=left,
		breaklines=false,
		basicstyle=\footnotesize\ttfamily, 
		caption={Fitted ARIMA}, % Caption above the listing
		frame=single,
        numberstyle=\tiny,
        columns=flexible,
        backgroundcolor=\color{backcolour}\ttfamily,
    ]{Listings/arima_results.txt}
\begin{flushright}
    \href{https://cutt.ly/0fhZM12}{(\underline{code available here})}
\end{flushright}
However, inspecting the residuals closely as in Figure ~\vref{fig:arima_residuals}\footnote[1]{I found that the value for Jarque-Bera statistics is too high here, it seems that it is a bug in the statistical package I used. I left it here as it makes little difference to the overall analysis.}, I note that the fit is not perfect. There is asymmetry in the residuals, since they are positive more often than negative. We also have some heavy tails in the residuals' distribution (again the positive tail is stronger) based on Q-Q plot as well as the kurtosis of the residuals. I managed to fully remove the autocorrelation in the process based on the Correlogram. Although, the model parameters are significant, the model fails to meet its specifications in Equation ~\vref{eqn:arima_model} as the residuals are strongly non-normal. There is a lot of noise in the 2-second data.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.36\textheight]{arima_residuals.png}
    \decoRule
    \caption{ARIMA Residuals Diagnostics}
    \label{fig:arima_residuals}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/0fhZM12}{(\underline{code available here})}
\end{flushright}

However, this model is not used in the bid and ask curve forecasting. It is presented here because I drew an important conclusion from this model as well as from the ADF test. Namely, the reason why the bid-ask spread series $s_t$ are stationary is, that the best bid and ask prices are cointegrated.

Fundamentally, cointegration is a form of statistical relationship whereby two or more time series show fairly stable or constant difference between each other over time. In the words of \cite{Lutkepohl2005}, they are driven by a common trend. In my settings, the cointegration makes sense economically as the order book prices all represent the values of one currency pair.

%----------------------------------------------------------------------------------------
%	SECTION 2 - VAR and VECM
%----------------------------------------------------------------------------------------

\section{VAR and VECM} \label{sec:vecm}

Mathematically, variables in a K-dimensional process $\boldsymbol{y_t}$ are called cointegrated of order ($d$, $b$), if all components of $\boldsymbol{y_t}$ are of some higher order of integration $I(d)$ and there exists a linear combination $\boldsymbol{z_t} := \boldsymbol{\beta'} \boldsymbol{y_t}$ with $\boldsymbol{\beta} = (\beta_1,\ \dots,\ \beta_K)' \neq \boldsymbol{0}$ such that $\boldsymbol{z_t}$ is of lower order if integration $I(d-b)$. For example, if all components $\boldsymbol{y_t}$ are of the order of integration $1$, $I (1) $, and $\boldsymbol{\beta'} \boldsymbol{y_t}$ is stationary (order of integration $0$, $I(0)$), then $\boldsymbol{y_t}$ is said to be cointegrated of order (1,1). The definition is taken from \cite{Lutkepohl2005}.

The system I work with, consists of 50 bid and 50 ask prices in ($\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$) with initally 2-second frequency, however, this frequency makes the data fairly irregular (see for example Figure ~\vref{fig:5_best_bid_ask_over_time}). Thus, I take approximately 1-minute intervals. This means that the number of rows in my dataframes with the sample reduces from 27,343 rows to 912 rows (912 rows for ask side and 912 rows for bid side).

I worked with the Sample II (see Section ~\vref{sec:definitions}) as this sample is longer. The reduced frequency leads to less noise in the data, \cite{Chen2019} have done the same. I used \texttt{statsmodels} by \cite{statsmodelsdw} again. The statistical theory is based on \cite{Lutkepohl2005}. Many mathematical definitions are taken from this book. The applications are well described in \cite{Lutkepohl2004}.

In the previous section, I used only one model and one possibly cointegrated relationship (I have carried out a formal test for this). Nevertheless, the sample contains $50^2-50$ possible cointegrated relationships on one side of the market. We need a framework to handle a large number of these. This framework is called Vector Error Correction Model, or VECM for short. However, the first step when estimating VECM is to actually start with Vector Autoregressive Model (VAR), following the procedure given in \cite{Lutkepohl2004}.

\subsection{Note on Notation}

The labels I used in this section are \textit{MinAsk1, MinAsk2, MinAsk3} and so on, indicating the price level $n$ step away from the spread. Similarly, the label \textit{MaxBid1, MaxBid2 or MaxBid3}, etc. are the closest bid prices to the bid-ask spread. They are more programmatic labels rather than mathematical ones.

%----------------------------------------------------------------------------------------
%	SECTION 2 - VAR
%----------------------------------------------------------------------------------------

\subsection{VAR Definition}

The definitoin for model of order $p$ VAR(p) using the notation for price sets $\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$ in Section ~\vref{sec:definitions} is provided in Equation ~\vref{eqn:VAR}. Note that capital $P$ marks prices and lower case $p$ marks the order of VAR.

For K-dimensional random vector $\boldsymbol{P_t} = (P_{1t},\ \dots,\ P_{Kt})'$, a VAR model captures the dynamic interaction in the system. The form is:
\begin{equation}
    \begin{split}
        \bm{P}_t = \bm{A_1} \bm{P}_{t-1} + \dots + \bm{A_p} \bm{P}_{t-p} + \bm{u_t} \quad \text{with} \quad \bm{u_t} \sim (\bm{0}, \bm{\Sigma}_u)
    \end{split}
    \label{eqn:VAR}
\end{equation}
$\bm{A_i}$'s are ($K \times K$) coefficient matrices and $\bm{u}_t = (u_{1t}, \dots, u_{Kt})$ is an unobservable error term. This is assumed to be zero-mean independent white noise with time-invariant, positive definite covariance matrix $\mathbb{E}[u_t u'_t] = \bm{\Sigma}_u$ (see \cite{Lutkepohl2005} for full definition).

\subsection{Fitting VAR}

Firstly, I note that all the prices that I work with have one degree of integration ($I(1)$). It is easy to show that if the prices are differenced once, they become stationary. I do not provide the formal tests. This is just a repetition of the ADF test above in Section ~\vref{sec:unit_root_test}.

When fitting VAR, it is a good idea to start with Granger Causation Test. The tests indicate the explanatory power of variables within the system. The test considers all variables as regressands and regressors at different stages of the test.

\subsection{Granger Causation Test}

The hypothesis is
\begin{gather*} \label{eqn:granger_causation_test}
    H_{0}: \text{all coeffients are statistically zero in a lagged regression}, \\
    H_{1}: \text{at least one of the coeffients is different from zero in the regression}.
\end{gather*}

I run the test for all 50 bid and 50 ask prices with up to 12th lag. The resulting p-values can be presented in a table, however, I was not able to fit the 2 tables (one for bid and one for ask prices) on an A4 page. The tables are available \href{https://cutt.ly/afkj7RD}{\underline{here}}\footnote[1]{\href{https://cutt.ly/afkj7RD}{https://cutt.ly/afkj7RD}, last accessed on \today} and \href{https://cutt.ly/rfkkadI}{\underline{here}}\footnote[2]{\href{https://cutt.ly/rfkkadI}{https://cutt.ly/rfkkadI}, last accessed on \today} to download in a CSV format.

In this report, I only present an example of the results.

\begin{table}[H]
    \caption{p-values for Granger Causality Test}
    \begin{adjustbox}{width=1\textwidth}
        \centering
        \input{Tables/gc_small_ask}
        \label{tab:gc_small_ask}
    \end{adjustbox}
\end{table}
\begin{flushright}
    \href{https://cutt.ly/Efkfquw}{(\underline{code available here})}
\end{flushright}

The table contains the p-values of $H_1$. The way to read the table is that the columns (prices labelled with "$\_x$") are causing the variable in the rows (labelled with  "$\_y$"). Thus, if the p-value is close to zero, we reject $H_0$, and have indicated that the explanatory variables (labelled "$\_x$") are relevant to the target variables (labelled "$\_y$").

I picked the variables, where Granger causation was statistically significant in the table. I aim to model full bid and ask curves (from Chapter ~\vref{Chapter3}) and so the subset in Table \ref{tab:gc_small_ask} is not sufficient for the full curve. I am testing the methods in this section.

I also tried to run the Granger tests with the series in the stationary form and the tests are indicating similar results. I use non-stationary form of the series of these tests.

\subsection{Selecting VAR Order}

\cite{Lutkepohl2004} suggest using AIC or other criteria to establish the number of the lags in the system, $p$, to use. The criteria values in provided in the Listing ~\vref{lst:var_order_selection}.

\vspace{5mm}

\lstinputlisting[
	label={lst:var_order_selection},
	basicstyle=\ttfamily,
	language=Python,
	numbers=left,
	breaklines=false,
	basicstyle=\footnotesize\ttfamily, 
	caption={Selecting VAR Order},
	frame=single,
	numberstyle=\tiny,
	columns=flexible,
	backgroundcolor=\color{backcolour}\ttfamily,
    ]{Listings/var_order_selection.txt}
\begin{flushright}
    \href{https://cutt.ly/Jfk02bN}{(\underline{code available here})}
\end{flushright}

In this case, the AIC suggests using lag order 9, however BIC suggests lag order 2. I run the below analysis for both lag orders and I found there was a little benefit in having VAR(9). I use the VAR(2) model as it is the simplier model.

\subsection{VAR Fit}

Having completed Granger Causation Test and selected lag order, I fitted the VAR(3) model to the system of prices used in Table \ref{tab:gc_small_ask}. The set of prices I use here are
\begin{equation}
    \begin{split}
        \bm{P}_t = (P^{(a)}_{t,1},\ P^{(a)}_{t,5},\ P^{(a)}_{t,6},\ P^{(a)}_{t,8},\ P^{(a)}_{t,9},\ P^{(a)}_{t,10},\ P^{(a)}_{t,11},\ P^{(a)}_{t,21}).
    \end{split}
    \label{eqn:VECM_fitted}
\end{equation}
The price set $\bm{P}_t$ is used in VAR in the differenced format ($\Delta \bm{P}_t$) to make the VAR stable. The model is stated in Equation ~\vref{eqn:VAR}, with $p=3$. Listing ~\vref{lst:var_result} for the model is in Appendix \ref{AppendixA}. A quick inspection shows that many of the model parameters are not significant. This tells us that the VAR (2) is not a good representation of the process. I checked the model forecasts. They are presented in Figure ~\vref{fig:var_forecast}. It is quite clear that the forecasts are not accurate.

I therefore try to run the VECM. The VAR model is an important step in building VECMs, see \cite{Lutkepohl2004}. It should tell us the lag order, but the evidence for lag order 2 is at very least mixed. The VECM should be better as it handles the cointegrated relationships.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.4\textheight]{var_forecast.png}
    \decoRule
    \caption{Var Forecasts}
    \label{fig:var_forecast}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/Jfk02bN}{(\underline{code available here})}
\end{flushright}


%----------------------------------------------------------------------------------------
%	SECTION 2 - VECM
%----------------------------------------------------------------------------------------

\subsection{VECM Definition}
The VECM model I decided to use for price forecasting is

\begin{equation}
    \begin{split}
        \Delta \bm{P}_t = \bm{\alpha} \bm{\beta}' \bm{P}_{t-1} + \bm{\Gamma} \Delta \bm{P}_{t-1} + \bm{u}_t,
    \end{split}
    \label{eqn:VECM}
\end{equation}
such that
\begin{equation}
    \begin{split}
        \bm{\alpha} := \begin{bmatrix} \alpha_1 \\ \alpha_2 \\ \vdots \\ n \end{bmatrix}, \bm{\beta}' := (1,\ -\beta_1,\ -\beta_2,\ \dots,\ -\beta_{n-1}) \quad \text{and} \quad  \bm{\Gamma} := \begin{bmatrix} \gamma_{11} & \gamma_{12} & \dots & \gamma_{1n}  \\ \gamma_{21} & \gamma_{22} & \dots & \gamma_{2n} \\ \vdots & & & \vdots \\ \gamma_{n1} & \gamma_{n2} & \dots & \gamma_{nn} \end{bmatrix},
    \end{split}
    \label{eqn:VECM_definitions}
\end{equation}
where the term $\bm{\alpha} \bm{\beta}' \bm{P}_{t-1}$ represents the error correction process, the long term relationship with the loading parameters $\bm{\alpha}$ as a measure of the correction. The parameters $\bm{\beta}'$ represent the linear combination between the non-stationary process ($I(1)$ processes) in the original form. The matrix $\bm{\Gamma}$ holds the parameters to establish the relationships between the stationarity and short-term differenced values of all the regressors in the system; this is the VAR part of the system. There are more complicated VECMs, however, I found that this form worked reasonably well. $\bm{u}_t$ is the same as in the VAR case.

My statistical package also allows for inclusion of various constants and linear trends in the VECM, however none of these were statistically significant. I also tried greater than first lags in the model, these did not add any more explanatory power to the model and were not statistically significant. The \texttt{statsmodels} package uses t-values based tests to determine statistical significance of the models.

$\Delta \bm{P}_t$ is the first difference of the a vector of some of the price series $P_{t,i}$ contained in $\mathbb{P}^{(a)}_t$ or in $\mathbb{P}^{(b)}_t$ with $i$ taking some values from $1,\ \dots, 50$. The prices are still ordered in the ascending order within the sets as well as the model.

\subsection{Fitting VECM}

I use the value of $n=6$ changes based on the VECM that was able to establish as reasonably statistically significant. The VECM, I chose as an example, is based on $\bm{P}_t \subset \mathbb{P}^{(a)}_t$ where $\bm{P}_t$ is
\begin{equation}
    \begin{split}
        \bm{P}_t = (P^{(a)}_{t,2},\ P^{(a)}_{t,7},\ P^{(a)}_{t,11},\ P^{(a)}_{t,17},\ P^{(a)}_{t,18},\ P^{(a)}_{t,23}).
    \end{split}
    \label{eqn:VECM_fitted}
\end{equation}
The model was fitted with the stationary form of $\bm{P}_t $.

We should use the same number of lags as in VAR, case (see \cite{Lutkepohl2004}), however, they are not statistically significant, so I took the second lag away. This makes the fit more statistically valid.

In addition, all possible cointegrated relationship estimates for ($\bm{\beta}'$) were statistically significant. However, when I increased the number of these relationships in the fit, it was causing the loading parameter estimates for $\bm{\alpha}$ to be not statistically significant.


\subsection{VECM Fit Results}

I ended up with a one cointegrated fitted vector ($\bm{\hat{\beta}}'$), which was strongly statistically significant. The estimated loading vector $\bm{\hat{\alpha}}$ was statistically significant up to 10\% significance level. The results of the model for price vector from Equation ~\vref{eqn:VECM_fitted} are presented in Listing ~\vref{lst:vecm_ask_inner}.

The problem is the short-term part (VAR part) of the model. Some of the parameter estimates in $\bm{\hat{\Gamma}}$ were statistically significant and some were not at the same time. The fit leads to conflicting decisions with regards to these parameter estimates.

This problem would need a restricted form of a VECM where we would be able to define specific parameters to be kept in the model. My package did not allow me to do that (not sure if R can handle that).

Nevertheless, I was able to fit four different VECM models to the system. These were a reasonable approximation for the evolution of the prices in $\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$ over time. I don't report the other 3 VECM models as they are almost identical to the one presented here (they have the same problem).

Figure ~\vref{fig:vecm} shows the cointegrated ask prices in $\bm{P}_t$.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.4\textheight]{vecm.png}
    \decoRule
    \caption{Statistically Cointegrated Prices}
    \label{fig:vecm}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/lflrvF1}{(\underline{code available here})}
\end{flushright}

\section{A quick ARIMA Model}

Looking at the Listing ~\vref{lst:var_result} closely, it can be noted that only the purely autocorrelated lags are statistically significant, which suggests that I should try something basic too, such as ARIMA fits. Using $P^{(a)}_{t, 5}$ from VAR model as an example, it is easy to show that
\begin{gather} 
    \Delta P^{(a)}_{t,5} = \phi_1 \Delta  P^{(a)}_{t-1,5} + \phi_2 \Delta  P^{(a)}_{t-2,5}  + \epsilon_t \quad \quad \epsilon_t \sim N(0, \sigma^2).
\label{eqn:arima_model}
\end{gather}
is statistically significant. Listing ~\vref{lst:arima_price} in the Appendix \ref{AppendixB} has the results. The fit seems reasonable as per Figure ~\vref{fig:arima_price} and it is consistent with the VAR fit in Listing ~\vref{lst:var_result}, where the two AR terms are significant.

We could use this ARIMA model framework in some sense, however, we would have to find 50 of them to carry out forecasting for ask curve and 50 for bid curve. The problem is that we are not able to capture the cointegration by these models, since the model copes only with one variable at a time.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.4\textheight]{arima_price.png}
    \decoRule
    \caption{Basic ARIMA for One of the Ask Price}
    \label{fig:arima_price}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/Hfli443}{(\underline{code available here})}
\end{flushright}

\section{Final Remarks}
The VAR model did not prove to be useful to handle the system. However, it is a useful building block for VECM. The VECM has a strong potential, however, it requires restricted and more detailed estimation than what I have done here. The ARIMA fits could be used as a sanity check for forecasts from VECM. The Granger Causation Tests were not very helpful.

In order to forecast the prices for the bid and ask curves, I would fit a number of VECM models and use these. The system of 50 bid and 50 ask prices are too complicated to be handled by a single VECM.

I have discussed some forecasting techniques for the price sets $\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$ in this chapter. In order to handle the full forecast for the system, we need a method to model the quantities $Q^{(a)}_t(P)$ and $Q^{(b)}_t(P)$. The next chapter focus on this.

\let\cleardoublepage\clearpage