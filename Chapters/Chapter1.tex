
\chapter{Introduction} % Main chapter title

\label{Chapter1} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION Introduction
%----------------------------------------------------------------------------------------

One of the most known and used concepts in economics are supply (ask) and demand (bid) curves showing the willingness of sellers to trade at a specific price as an increasing function of quantity. Similarly, following this concept, buyers are willing to trade at specific prices as a decreasing function of quantity (volume). This idea of supply and demand curves is fairly old and naive, as it used only linear relationships originally. We know that almost all economic relationships are probabilistic by nature, not deterministic.

In this project I used crypto currency data to examine and fit curves for the bid and ask prices with their corresponding volumes. The aim is to find the equilibrium price between the two fitted curves, the equilibrium volume is secondary in the project. I estimate the equilibrium as an intercept of the fitted curves. Secondly, the project covers probabilistic forecasting of the fitted supply and demand curves and, hence, the forecasting of equilibrium.

I focus on finding statistically significant relationships to achieve these objectives. The statistical learning or non-parametric methods are not covered in this project.

The reason for working with the market order data is to try to understand the underlying market structure and use that structure to explain the market price behaviour and forecasting. I used crypto currency data, however the methodology I propose in this project can be used for any market that is similar in the structure.

The project objectives are:
\begin{itemize}
    \item to find the best method to estimate the market equilibrium for the given order book dataset,
    \item to find suitable methods to forecast the prices within the order book,
    \item to find suitable methods to forecast the volumes within the order book and
    \item to construct the equilibrium price forecest using the forecasted prices and volumes.
\end{itemize}

%----------------------------------------------------------------------------------------
%	SECTION Literature Overview
%----------------------------------------------------------------------------------------


\section{Literature Review} \label{sec:lit_overview}
In order to better understand all the issues involved and to make sure that I follow the correct and logical approach to my selected problem, I have looked at numerous articles some of which are summarised below.

\subsection{Theoretical Models}

\cite{Orrell2020} provides a suggestion how the simple linear model for bid and ask curves can be improved. His work is purely theoretical and the author makes many assumptions around the proposed probabilistic model.

Firstly, he considers a market with one buyer considering bid price $\mu_b$ and one seller thinking about asking price $\mu_a$. The author assigns normal distributions to prices at which both seller and buyer are willing to trade, by assumptions. Using his notation, $P_a(x)$ is the normal PDF for the seller and $P_b(x)$ is the normal PDF for the buyer where $\mu_b < \mu_a$. $\sigma_a^2, \sigma_b^2$ represent the willingness to negotiate the transaction price for the seller and the buyer, respectively.

This model in the \cite{Orrell2020} paper is used as a building block. The interpretation is that each seller has a specific price to trade for in mind - $\mu_a, \mu_b $. Their willingness to negotiate based on the dispersion parameters $\sigma_a^2, \sigma_b^2$ gives rise to the transaction probability as a product of the two previously mentioned probabilities. This is identical to finding the posterior for two normal distributions.

Following this, the author defines the theoretical model for a number of sellers and number of buyers with the assumptions that all buyers and all sellers share the same parameters - mean and standard deviation. Thus, the author is using cumulative normal probability densities for the sellers and buyers, respectively, to establish the willingness to trade. These densities are scaled by the number of sellers and buyers.

My interest in this article lies in the fact that cumulative probability densities resemble the simple linear supply and demand curves. The bid cumulative curve is counted from the total number of buyers in this model to make sure that is downwards slopping and inversely cumulative. Orrell then works with the derivatives to find the equilibrium price. This is then followed by modelling the movements around the equilibrium with SDEs.

The model is a nice theory, however the author leaves little clues with regard to validation of the theory. We are not given any statistical significance tests or accuracy. The points to take from us is to definitely try to fit supply and demand curves as non-linear functions. The problem seems to be the number of assumptions the this author had to use to make the model work.

\vspace{5mm}

Another approach was taken by \cite{Goettler2005}. This paper seems to be a litte dated and it was published in a finance focused journal. These authors took a completely different approach and aimed to fit a Markov chain to the order book dynamics. The model was purely theoretical, which again leads to number of assumptions and unsolved problems of, for example, finding distribution of the quantities the trader would provide to the market order book. Instead, they made assumptions about the Markov chain input properties. Doing so, they were able to approximate all market order book operations as they happen over time.

Specifically, the assumed so-called consensus value (that is the vale that lies between the possible bid and ask prices, which I call equilibrium in my project) is known to all traders and all of them would aim to trade at this price or place a limit order, that is either higher or lower based on their view. This order is not imidiatly traded but stays as a part of the order book.

\cite{Goettler2005} in their paper provide the example input values for the Markov chain and they used Monte Carlo simulations to approximate trade arrivals to find Markov Equilibrium. Nevertheless, they made no attempt to actually estimate the so-called consensus value.

\subsection{Theoretical Models with Empirical Aspects}

Yet a different attempt at modelling a full market order book is by \cite{Kononovicius2019a}. They provide an approach to fully parameterise the order book modelling problem. Not only do they parameterise the order book, but they also attempted to model market participants with herding behaviour.

They constructed the market order book ask curve via the addition of gamma distributed values. Similarly, the bid curve was constructed via subtraction of gamma distributed values from the current valuation. Interestingly, the values for the parameters of the gamma distribution was just used with reference to another paper, no statistical test was performed even though they had Bitcoin and some stock data.

This does not match the aims of my project. The valuation of the instrument is never zero and the, as I called it, the equilibrium price is not directly observable and must be estimated. It is hard to see how one can start building market order book from unobservable terms. \cite{Kononovicius2019a} made assumptions around this point.

The main objective taken up by \cite{Kononovicius2019a} was to establish a full market model with parameters such as the number of agents and switch rates between different type of herds. They also compared some of the statistical properties of the series they had available to the values generated by their model in order to justify their settings. They have not considered overfitting or coincidental match in their analysis.

They made a few interesting points about market order book structure and possible evolution of the market. They used gamma distribution to model the book structure.

\vspace{5mm}

\cite{Meudt2016} focused on modelling supply and demand within the market order book setting, however they created a market model based on assigning parametric functions to trades with different roles in the market. The trades were randomly generated to simulate the trading activity. They used specific parametric functions to represent the market order book supply and demand.

These authors validated their model by comparing statistical properties from their model to the observed properties. Namely, they compared the normalised returns to the normal distribution and they found that their model was generating heavier tails. The volatility generated by their model followed log-normal distribution, which is what they expected with reference to \cite{Micciche2002}. They found some expected autocorrelations in the generated returns, which was originally found in \cite{Cont2001}.

One of the strongest conclusions \cite{Meudt2016} made was that there is no need to find any equilibrium prices for a market operating under market order book. They were able to generate returns with reasonable statistical properties using their model. However, it should be noted that \cite{Meudt2016} did not carry out formal statistical tests or they have not even attempted to calculate the accuracy of the model. This study was mostly theoretical with a number of assumptions.


\subsection{Empirical Models}

Instead of creating theories about a order book market, \cite{Soloviova} provide a method to approximate full supply and demand curves in the Italian electricity market.

Their data consisted of some 320 ask quotes and 70 bid quotes on per-hour basis. However, by the structure of the Italian electricity market, the quotes are provided significantly below and above the equilibrium for both supply and demand curves. Their interpretation of the equilibrium is the intercept of the two curves. The curves are monotonostic by nature. For the crypto currency data, the quoted prices are always only provided below equilibrium for ask (supply) curve and always only above the equilibrium for bid (demand) curves.

\cite{Soloviova} used radial basis function with integral of Gaussian function as the error function. The whole problem was too complex so they split up the whole supply and demand curves into smaller sets and they used the fitting mechanism on the smaller subsets of the curves. They did not provide detailed explanation about the algorithm they used to split up the curves.

The approach these authors took would not work for my project, as only half of the supply and demand curve is observable in the crypto currency market. I would not be able to extend the approximation slightly out of the sample.

\vspace{5mm}

The paper by \cite{Ziel2016} also used the data from the electricity markets (the European and the German markets). It should be noted that the electricity data are hourly and the possible tradable prices are given by the regulators. The hourly and consistent frequency is not found in the crypto currency data. In addition, crypto currency does not have a clear possible set of prices to use. Hence, the work by \cite{Ziel2016} is not directly applicable to my dataset.

Nevertheless, they constructed the supply and demand curve as a cumulative sum of the quoted volume. They called their model the X-Model as the curve look like an X. I use this cumulative sum too, however my model would be called V-Model using an analogy to their model. Only half of the supply and demand curves are quoted in the crypto currency market. The reason for this is the what the exchange market works. In addition, the pointed end of the letter V would be the equilibrium that I need to estimate. They used linear interpolation to build the full curves to establish the equilibrium. I am not able to do this using Bitcoin data, as I need to estimate the relationships slightly out of sample to find the equilibrium.

However, I took a lot of ideas from this paper. As \cite{Ziel2016} did need to forecast the possible prices, they built a model for forecasting the volume. They used linear regression relationships of the current value of the volume based on up to six lagged values within a given class of the volume levels. In addition, they added to their multivariate linear model some regressors that are known to have explanatory power for the electricity prices or volume. They used LASSO estimator as they had a large number of regressors. In my case, I have to forecast both the prices and the volume to find the estimated future curves. I could not take many mathematical techniques from this paper.

\vspace{5mm}

\cite{Weron2014} provides overview of the different methods used in electricity price forecasting. The methods used in the literature are multi-agent (which simulate the operation of the system of the market participants), fundamental (which focus on impact of important economic or physical factors), reduced-form (which use the statistical properties of the electricity prices with the aim of risk management), statistical (which use statistical techniques) and finnaly computational intelligence (artificial intelligence, non-parametric methods). Any of the methods could be used for my project too. I focus on the statistical methods. Weron was one of the authors for \cite{Lago2020} where the authors provided a forecast accuracy comparison of a number of methods, however \cite{Lago2020} is a too recent paper to be peer reviewed at the time of this report.

\vspace{5mm}

Another interesting paper I came across was by \cite{Chen2019}. Their model was handling fitted supply and demand curves with stock exchange limit market order book data. The interesting aspect is that they took the log of the quantities to smoothen the jumps in the curves and, also, they used 5-minute interval data for 12 different US-listed stocks over 44 days with 100 quotes on each side of the market.

They established so-called Vector Functional Autoregressive (VFAR)  model for the supply and demand curves. They worked with Sieve Maximum Likelihood estimator for the model. They derived a single model to handle the serial dependency as well as correlations within the system of large dataset they used. They also showed the consistency properties of the estimator they used. Their approach gave them model fits with 98.2\% $R^2$ in out-of-sample experiments with a very accurate forecast for up to 10 steps ahead (50 minutes ahead).

I used this technique of reducing the dataset, however the study is too recent and the mathematics is not covered in any text books. I also expect that there are no statistical packages available to handle their method.


%----------------------------------------------------------------------------------------
%	SECTION Background
%----------------------------------------------------------------------------------------

\section{Coding Work for the Project} \label{sec:background}

I carried out a lot of coding for collecting and storing the data sets. The work can be accessed at \href{https://gitlab.com/Radek-dev/pyl2handler}{Gitlab} with the link \cite{ProjectRepo} where the project website is based.

I built a Python package to hold all the project's code. The \href{https://shorturl.at/BHJQ9}{websocket client} to connect to the crypto currency exchange was difficult to handle as it uses asynchronous programming.
I have a small Postgres \href{https://shorturl.at/suvzW}{database} running as an AWS instance for this project with its \href{https://shorturl.at/grU35}{queries}.

The code base is not printed in this report, but it can be accessed on the project website or by following the links embedded in the report. Each table, figure and model have a link attached to the code.


%----------------------------------------------------------------------------------------
%	SECTION Project Structure
%----------------------------------------------------------------------------------------


\section{Report Outline}

The introduction to the problem is provided in Chapter \ref{Chapter1}. Chapter ~\vref{Chapter2} provides the exploratory analysis and many visualisations. Chapter ~\vref{Chapter3} focuses on finding the statistically significant equilibrium for the dataset. I then work on price forecasting in Chapter ~\vref{Chapter4} using time series models. Chapter ~\vref{Chapter5} aims to solve the forecasting problem for the quantities associated to each quote. Finally, I draw some conclusions in Chapter ~\vref{Chapter6} and outline further work possibly needed. I also used a number of Appendices in which I have included usually computer outputs used in Listings though this report. These can be found at the end of this report and I refer to them througout.


%----------------------------------------------------------------------------------------
%	SECTION Limit Order Book - Definitions
%----------------------------------------------------------------------------------------

\section{Limit Order Book - Definitions and Notation} \label{sec:definitions}

This project focuses on modelling data generated by a limit order book. The order book holds both buy (bid) and sell (ask) orders for Bitcoin. Orders for other currencies can be accessed there as well. There are two main types of orders: market orders and limit orders. A market order is an order to buy (or sell) immediately, typically at the best ask price (or bid price). Limit orders specify the price to trade at and so they stay in the limit order book until they are matched by market orders.

In addition to the price, each market order specifies the quantity to be traded. In case of the Bitcoin-USD currency pair, the quantity is a percentage of one Bitcoin to be traded. If market order is not fully matched in terms of the quantity, it stays in the book as a limit order. In some markets, a market order can be matched at different prices as it matches a number of limit orders.

The set of limit order books is called the depth of the limit order book and it can be organised in different price layers. This market type is called continuous market auction. The crypto currency market is open 24 hours a day and every day.

I collected the outstanding limit orders from \href{https://cex.io}{Cex.io} exchange for USD per 1 BTC currency pair.

I focused on working with so-called Level 2 data. This data has a number of different ask and bid prices at each time point. Cex.io exchange provides an interesting animation for the data generating process \href{https://cex.io/btc-usd}{on their website} \footnote[1]{\href{https://cex.io/btc-usd}{https://cex.io/btc-usd}, last accessed on \today}.

A small example of the data set used for this project is presented in Tables ~\vref{tab:data_ask} and ~\vref{tab:data_bid}.


\begin{table}[H]
    \caption{Sample Ask Quotes for One Time Period}
    \centering
    \input{Tables/data_ask}
    \label{tab:data_ask}
\end{table}
\begin{flushright}
    \href{https://cutt.ly/lffVx96}{(\underline{code available here})}
\end{flushright}

\begin{table}[H]
    \caption{Sample Bid Quotes for One Time Period}
    \centering
    \input{Tables/data_bid}
    \label{tab:data_bid}
\end{table}
\begin{flushright}
    \href{https://cutt.ly/lffVx96}{(\underline{code available here})}
\end{flushright}

Both above Tables ~\vref{tab:data_ask} and ~\vref{tab:data_bid} display data that represent bid and ask quotes submitted to market order book at a specific time. In this report, I labelled this time as \textit{my\_time}, that is the time my computer received the data. Time was recorded only with second resolution. \textit{Quote\_type} is either bid or ask. \textit{Book\_id} is the unique identifier in the state of the order book. \textit{Price} is the quoted price, stated in USD per 1 BTC. \textit{Quantity} is the percentage of 1 BTC to trade, for instance for line 0 in Table \ref{tab:data_bid} the total quoted value in USD is $9150.2 \times 0.00516 = 47.215$ USD. These are the limit orders outstanding in the book. The price-quantity quotes are used for the analysis in this project.

The price grid for the process can be defined as a set $\mathbb{P} = \{0.1,\ 0.2,\ 0.3,\ \dots\}$ of all the possible prices. These possible prices for the currency pair should not be zero, as zero would not make economic sense. The maximum prices are directly observable as they would represent how much someone is willing to pay at most for the currency pair. Also, the price grid is discrete. This perhaps arises as due to the computer limitations.

Further, I define the quoted quantity at each price $P \in \mathbb{P}$ as $Q^{(a)}_t(P)$ for the ask (sell) quantity and as $Q^{(b)}_t(P)$ for the bid (buy) quantity at the time $t$. As previously suggested, the quantity grid for the BTC/USD currency pair can be defined either as a percentage to the price quote or the as the product of the percentage to the price. Either case, the quantities are positive numbers and closer to the continous numbers.

Furthermore, I establish the set of ask price $\mathbb{P}^{(a)}_t$ and bid price $\mathbb{P}^{(b)}_t$ such that
\begin{equation}
    \begin{split}
        \mathbb{P}^{(a)}_t = \{ P \in \mathbb{P} | Q^{(a)}_t(P) > 0 \} \quad \text{and} \quad \mathbb{P}^{(b)}_t = \{ P \in \mathbb{P} | Q^{(b)}_t(P) > 0 \}
    \end{split}
    \label{eqn:price_sets}
\end{equation}
In case of the currency pair I used for the project, the properties of these two sets are that all prices in $\mathbb{P}^{(b)}$ are always smaller than the prices in $\mathbb{P}^{(a)}$ inrespective of time. Also, $\mathbb{P}^{(a)} \cap \mathbb{P}^{(b)} = \emptyset \quad \forall t$ while $\mathbb{P}^{(a)}_t \cup \mathbb{P}^{(b)}_t$ represents the current state of the order book (marked by the \textit{book\_id}). The union changes each time there is a market event in the market book.

The minimum (best) ask price, $P^{(a)}_{t, min}$,
\begin{equation}
    \begin{split}
        min \left[ \mathbb{P}^{(a)}_t \right] = P^{(a)}_{t, min}
    \end{split}
    \label{eqn:price_min_ask}
\end{equation}
and maximum (best) bid price, $P^{(b)}_{t, max}$,
\begin{equation}
    \begin{split}
        max \left[ \mathbb{P}^{(b)}_t \right] = P^{(b)}_{t, max}
    \end{split}
    \label{eqn:price_max_bid}
\end{equation}
are significant as these are the prices commonly shown as the actual price of the currency pair, however this price quote disregards the depth of the order book, meaning that it ignores the additional bid and ask limit orders in the market book.

The data that was available to me has 50 bid and 50 ask quotes so
\begin{equation}
    \begin{split}
        \mathbb{P}^{(a)}_t = \{ P^{(a)}_{t,1},\ P^{(a)}_{t,2}, \dots, P^{(a)}_{t,50} \} = \{ P^{(a)}_{t, min},\ P^{(a)}_{t,2}, \dots, P^{(a)}_{t,50} \},
    \end{split}
    \label{eqn:price_set_ask}
\end{equation}
and
\begin{equation}
    \begin{split}
        \mathbb{P}^{(b)}_t = \{ P^{(b)}_{t,1},\ P^{(b)}_{t,2}, \dots, P^{(b)}_{t,50} \} = \{ P^{(b)}_{t,1},\ P^{(b)}_{t,2}, \dots, P^{(b)}_{t,max} \}.
    \end{split}
    \label{eqn:price_set_bid}
\end{equation}
I work with the sets in an ordered fashion. The prices are ordered from the lowest to the highest values.

I further define \textit{mid price}, $P_{t, mid}$, as
\begin{equation}
    \begin{split}
        P_{t, mid} = \frac{P^{(a)}_{t, min} - P^{(b)}_{t, max}}{2}
    \end{split}
    \label{eqn:price_mid}
\end{equation}
However, it should be noted that $P_ {t, mid} $ is not necessarily in $\mathbb{P}$. Mid price is used in Section ~\vref{sec:non_linear_fits} as a part of transformation for a non-linear fit.

The \textit{bid-ask spread}, $s$, is the difference between the best bid price and the lowest ask price.
\begin{equation}
    \begin{split}
        s_t = P^{(a)}_{t, min} - P^{(b)}_{t, max}
    \end{split}
    \label{eqn:price_bid-ask_spread}
\end{equation}
The example of this would be the difference in price for lines 0 in both Tables ~\vref{tab:data_ask} and ~\vref{tab:data_bid}, specifically $9161.1 - 9150.2 = 10.9$ USD per BTC.

Tables ~\vref{tab:sample_summary} and ~\vref{tab:sample_summary4} provide a high level overview of the data set I used as a sample. I use two data samples collected on 28th June 2020 and on 12th-13th August 2020. The price at that time was around 9160 and 11600 USD per BTC respectively. I look at these as two separate samples, Sample I and Sample II. Sample I was collected over about 39 minutes, while Sample II was accumulated over some 15 and half hours. Each observation is produced in approximately 2 second time interval and it consists of 50 bid price-quantity pairs and 50 ask price-quantity pairs. The time intervals for each order book iteration are not equally spaced.

\begin{table}[H]
    \caption{Sample I Characteristics}
    \centering
    \input{Tables/sample_summary}
    \label{tab:sample_summary}
\end{table}
\begin{flushright}
    \href{https://cutt.ly/dffVlw0}{(\underline{code available here})}
\end{flushright}

\begin{table}[H]
    \caption{Sample II Characteristics}
    \centering
    \input{Tables/sample_summary4}
    \label{tab:sample_summary4}
\end{table}
\begin{flushright}
    \href{https://cutt.ly/OffVjxl}{(\underline{code available here})}
\end{flushright}

%----------------------------------------------------------------------------------------
%	SECTION - Simplifications and Assumptions
%----------------------------------------------------------------------------------------

\section{Simplifications and Assumptions} \label{sec:simplifications_assumptions}

First step in working with the quoted quantities $Q^{(a)}_t(P)$ and $Q^{(b)}_t(P)$ is to construct the supply and demand curves. This requires an assumption about ask quantities and an assumption about bid quantities.

I assume that if someone is willing to sell at lower ask price $P^{(a)}_{t,i}$, they will be willing to sell at higher ask price $P^{(a)}_{t, j > i}$. As discussed in later section, I had to go that far, that I allow this assumption to apply even if the higher ask price $P^{(a)}_{t, j > i}$ is not observed in my sample, however, is available in the overall price set $\mathbb{P}$. Such prices might occur when forecasting. This assumption allows for the construction of the supply curve.

An example of such assumption might be based on the Table ~\vref{tab:data_ask}. I assume that if someone is wishing to sell at 9161.1 USD per BTC, they will wish to sell at higher price of 9170.1 USD per BTC as well. Hence, we can add quantity associated with the first price level to the quantity associated with the higher quoted price.

Similarly, I assume that if someone is willing to buy at higher price $P^{(b)}_{t,i}$, they will be willing to buy at a lower price $P^{(a)}_{t, j < i}$. These assumptions are indentical. I construct the demand curve using these assumptions.

These assumptions also are in the academic papers related to this topic, such as \cite{Ziel2016} or \cite{Chen2019}, however, I do not see as a justification for my assumptions. I have to make these assumptions, since otherwise the modelling presented in my project would not work.

The ask and bid curves are constructred as

\begin{equation}
    \begin{split}
        A_t(P) =
            \sum_{\substack{p \leq P \\ \forall p \in \mathbb{P}^{(a)}_t}} Q^{(a)}_t(p) \quad \forall P \in \mathbb{P}^{(a)}_t
        \quad \text{and} \quad
        B_t(P) =
            \sum_{\substack{p \leq P \\ \forall p \in \mathbb{P}^{(b)}_t}} Q^{(b)}_t(p) \quad \forall P \in \mathbb{P}^{(b)}_t.
    \end{split}
    \label{eqn:cum_sum_ask_bid}
\end{equation}
where $A_t(P)$ marks the cumulative quantity upto the level given by $Q^{(a)}_t(P)$ and $B_t(P)$ is the cumulative quantity upto the level $Q^{(b)}_t(P)$.

I make one more assumption. I assume that the quantities are determing prices. This may not be the case necessarily. Reverting this assumption, it might be possible that prices determine the quantities. However, the ask and bid curves as defined in Equation ~\vref{eqn:cum_sum_ask_bid} are monotonic. So, it is reasonable to expect that the causality direction makes only a little difference. I used this assumption in Chapter ~\vref{Chapter3}.

The notation used was adapted based on \cite{Ziel2016}.

\let\cleardoublepage\clearpage
