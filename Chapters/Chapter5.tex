% Chapter Template

\chapter{Quantity Forecasting} % Main chapter title

\label{Chapter5} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

I aim to describe possible methods for quantity forecasting in this chapter. Section ~\vref{sec:quantity_analysis} has many related plots of the quantity behaviour in the order book system. Particularly, Figure ~\vref{fig:cum_qty_over_time} is a good representation of the system.

The idea here is, that we obtain a forecasted price $\hat{P}_{t+1,i}$ using the methods discussed in the previous chapter and we need the forecasted accumulated quantity $\hat{A}_t(\hat{P}_{t+1,i})$ or $\hat{B}_t(\hat{P}_{t+1,i})$ to match the price.

It should be noted that the price will be a real number ($\hat{P}_{t+1,i} \in \mathbb{R}$), however the price grid $\mathbb{P}$ is discrete. In fact, the forecasted price may not be in the current price sets $\mathbb{P}^{(a)}_t$ and $\mathbb{P}^{(b)}_t$. Therefore, I use the assumptions discussed in ~\vref{sec:simplifications_assumptions} to establish the cumulative volume almost over the whole $\mathbb{P}$ where the Equation ~\vref{eqn:cum_sum_ask_bid} would be valid but have zero volume.

The procedure would be to obtain the forecasted price $\hat{P}_{t+1,i}$ and establish the cumulative volume series for that price. I use the series to forecast few step ahead of quantity. Following the above logic, the problem becomes how to establish a time series model for the cumulative quantities at a given price level.

\section{Initial Analysis} \label{sec:qty_series_init}

I picked up the accumulated series as shown in the below Figure ~\vref{fig:cum_qty_over_one_price} to begin with. It seems though there are two processes happening at the same time. It is like the series was stationary, but there were two possible means. The jumps in the series are caused by the fact that the process is established as a sum of the quantities at lower prices.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.20\textheight]{Figures/cum_qty_over_one_price.png}
    \decoRule
    \caption{Accumulated Quantities for a Specific Price in Sample II}
    \label{fig:cum_qty_over_one_price}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/tfzKR45}{(\underline{code available here})}
\end{flushright}

This series is not stationary based on the ADF test results (Appendix \ref{AppendixC}, Section ~\vref{sec:apx_qty_series_init}). I require a model that handles different regimes in the series. The analysis suggests that I should consider Markov Switching models.

\section{Markov Switching Autoregression Model}

This theory was established by \cite{Hamilton1989}.

\subsection{Definition}

I was able to establish a statistically significant model for the series from Figure ~\vref{fig:cum_qty_over_one_price}. The model is
\begin{equation}
    \begin{split}
         A_{t} = \mu_{S_t} + \phi_1 (A_{t-1} - \mu_{S_{t-1}}) + \phi_2 (A_{t-2} - \mu_{S_{t-2}}) + \epsilon_t,\quad \quad \epsilon_t \sim N(0, \sigma^2),
    \end{split}
    \label{eqn:adf_model}
\end{equation}
with state space $S_t \in \{0,1\}$ and the regime transition probabilities as
\begin{equation}
    \begin{split}
        P(S_t = s_t | S_{t-1}=s_{t-1}) = \begin{bmatrix} p_{00} & p_{10} \\ 1-p_{00} & 1-p_{10} \end{bmatrix}
    \end{split}
    \label{eqn:VECM_definitions}
\end{equation}
where $A_{t}$ denotes the specific cumulative volume for the price as in Figure ~\vref{fig:cum_qty_over_one_price}. $\mu_{S_t}$ are model's constants. Parameters $\phi_1$ and $\phi_2$ ensure the autoregression within the process. In this case, the order of autoregression two was statistically significant. $\epsilon_t$ is the error term in the model. Finally, the model estimates two regimes switching probabilities $p_{00}$ and $p_{10}$. $p_{00}$ estimates the probability of staying the state $s_t=0$, while $p_{10}$ estimates the probability of switching from $s_t=1$ to $s_t=0$ state.

\subsection{Model Fit}

The model fit is presented in Listing ~\vref{lst:markov_switching_good}. In order to find the fit, I actually re-run the model on the series a couple of times and removed the statistically not significant estimates. I used the 1-minute data from Sample II.

\lstinputlisting[
        label={lst:markov_switching_good},
        basicstyle=\ttfamily,
		language=Python,
		numbers=left,
		breaklines=false,
		basicstyle=\footnotesize\ttfamily, 
		caption={Fitted Markov Switching Model}, % Caption above the listing
		frame=single,
        numberstyle=\tiny,
        columns=flexible,
        backgroundcolor=\color{backcolour}\ttfamily,
    ]{Listings/markov_switching_good.txt}
\begin{flushright}
    \href{https://cutt.ly/tfzKR45}{(\underline{code available here})}
\end{flushright}

I am aware, that there are a few problems with my approach. I picked the series on which I run the model by hand. Therefore, by doing this, I introduced a bias, which cannot really be quantified by statistics computed here. To fix this, I ran the model on different series of cumulative volumes and the results were similar, but not as clear as this fit. There is a lot of noise in the data and sometimes the methodology does not handle this very well. Perhaps, this could be fixed by working with, say, 5-minute series.

\subsection{Model Interpretation}

The model estimates two regimes with means $\hat{\mu}_{0}=7.35$ and $\hat{\mu}_{1}=10.09$. These are the values around which the regimes evolve. The probability of staying at state 1 is $0.963$, thus we see that the model stays most of the time in this state only occasionally jumps to state 0. We can see the estimated evolution of the probability of staying in state 0 in Figure ~\vref{fig:markov_qty_p00}.

\begin{figure}[H]
    \centering
    \includegraphics[width=1\columnwidth, height=0.25\textheight]{Figures/markov_qty_p00.png}
    \decoRule
    \caption{Probability of Staying at State 0}
    \label{fig:markov_qty_p00}
\end{figure}
\begin{flushright}
    \href{https://cutt.ly/tfzKR45}{(\underline{code available here})}
\end{flushright}

\section{Final Remarks}

I have shown how the accumulated volume ${A}_t(\hat{P}_{t+1,i})$ or $B_t(\hat{P}_{t+1,i})$ in the market order setting can be modelled. However, it seems there are many tasks outstanding. I would like to have found vector representation of the volume time series. The problem is that the order book setting requires us to forecast a large number of different cumulative quantities at the same time.

\let\cleardoublepage\clearpage