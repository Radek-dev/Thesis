                            OLS Regression Results                            
==============================================================================
Dep. Variable:                  price   R-squared:                       0.971
Model:                            OLS   Adj. R-squared:                  0.970
Method:                 Least Squares   F-statistic:                     786.2
Date:                Tue, 21 Jul 2020   Prob (F-statistic):           7.49e-37
Time:                        14:56:42   Log-Likelihood:                -181.00
No. Observations:                  50   AIC:                             368.0
Df Residuals:                      47   BIC:                             373.7
Df Model:                           2                                         
Covariance Type:            nonrobust                                         
====================================================================================
                         coef   std err         t      P>|t|      [0.025      0.975]
------------------------------------------------------------------------------------
cum_quantity_ask^2    -0.4699     0.110    -4.263      0.000      -0.692      -0.248
cum_quantity_ask      18.9197     1.414    13.381      0.000      16.075      21.764
const_ask           9158.5112     3.117  2937.791      0.000    9152.240    9164.783
==============================================================================
Omnibus:                        2.822   Durbin-Watson:                   0.775
Prob(Omnibus):                  0.244   Jarque-Bera (JB):                2.537
Skew:                           0.545   Prob(JB):                        0.281
Kurtosis:                       2.825   Cond. No.                         164.
==============================================================================

Warnings:
[1] Standard Errors assume that the covariance matrix
of the errors is correctly specified.
